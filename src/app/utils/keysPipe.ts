import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      let valor = value[key]
      if (valor !== undefined && valor !== null && key !== "rankingType") {
        keys.push(key);
      }
    }
    return keys;
  }
}
