export class FontsHelper {

  static LEAGUE_GOTHIC = 'league-gothic';
  static ATARI = 'atari';
  static ARCADE_CLASSIC = 'arcade-classic';

  static getFonts(): string[] {
    const keys = Object.keys(this);
    const list = [];
    keys.forEach(key => {
      const descriptor = Object.getOwnPropertyDescriptor(this, key);
      list.push(descriptor.value);
    });
    return list.sort();
  }
}
