export class IconsHelper {
  static DEATH = {iconName: 'death', gameIcon: true};
  static EDIT = {iconName: 'edit', gameIcon: false};
  static GAME_CONSOLE = {iconName: 'game-console', gameIcon: true};
  static HANDGUN = {iconName: 'handgun', gameIcon: true};
  static HEALTH_POTION = {iconName: 'health-potion', gameIcon: true};
  static LADDER = {iconName: 'ladder', gameIcon: true};
  static LIFE = {iconName: 'life', gameIcon: true};
  static MANA_POTION = {iconName: 'mana-potion', gameIcon: true};
  static NINTENDO_SWITCH = {iconName: 'nintendo-switch', gameIcon: true};
  static PLAYSTATION = {iconName: 'playstation', gameIcon: true};
  static PS4_CONTROLLER = {iconName: 'ps4-controller', gameIcon: true};
  static RANKING = {iconName: 'ranking', gameIcon: false};
  static RETRO_CONSOLE = {iconName: 'retro-console', gameIcon: true};
  static SCORE = {iconName: 'score', gameIcon: false};
  static SCORE_CONTROLLER = {iconName: 'score-controller', gameIcon: false};
  static SCOREBOARD = {iconName: 'scoreboard', gameIcon: false};
  static SHIELD = {iconName: 'shield', gameIcon: true};
  static SPORTS = {iconName: 'sports', gameIcon: true};
  static STEERING_WHEEL = {iconName: 'steering-wheel', gameIcon: true};
  static SWORD = {iconName: 'sword', gameIcon: true};
  static USER = {iconName: 'user', gameIcon: false};
  static WARNING = {iconName: 'warning', gameIcon: false};
  static XBOX_CONTROLLER = {iconName: 'xbox-controller', gameIcon: true};

  static getIcons(): string[] {
    const keys = Object.keys(this);
    const list = [];
    keys.forEach(key => {
      const descriptor = Object.getOwnPropertyDescriptor(this, key);
      if (descriptor.value.gameIcon) {
        list.push(descriptor.value.iconName);
      }
    });
    return list.sort();
  }

  static getGameIcons(): string[] {
    const keys = Object.keys(this);
    const list = [];
    keys.forEach(key => {
      const descriptor = Object.getOwnPropertyDescriptor(this, key);
      if (descriptor.value.gameIcon) {
        list.push(descriptor.value.iconName);
      }
    });
    return list.sort();
  }
}
