import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Ranking} from '../models/ranking';
import {environment} from '../../environments/environment';
import {FormControl, Validators} from '@angular/forms';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;

  rankings: Ranking[];
  ignoredColumns: string[] = [];
  games: any[] = [];
  gameSelectControl = new FormControl('', Validators.required);
  gameSelected: any;

  constructor(private http: HttpClient) {
    this.blockUI.start();
  }

  ngOnInit(): void {
    this.http.get<any>(environment.apiUrl + '/rank', {
      params: {
        type: 'GAME'
      }
    }).subscribe((result) => {
      this.rankings = result.rankingList;
      this.ignoredColumns = result.ignoredColumns;
      this.blockUI.stop();
    });
  }

  changeTable(type): void {
    this.blockUI.start();
    this.rankings = [];
    this.ignoredColumns = [];
    this.games = [];
    this.gameSelected = '-';
    if (type.tab.textLabel === 'GAME') {
      this.getGamesRanks();
    } else if (type.tab.textLabel === 'GAME_MATCH') {
      this.getGames();
    } else if (type.tab.textLabel === 'USERS') {
      this.getUsersRanks();
    }
  }

  selectGame(game): void {
    this.blockUI.start();
    this.http.get<any>('http://localhost:8666/api/rank/' + game.id, {
      params: {
        type: 'GAME_MATCH'
      }
    }).subscribe((result) => {
      this.rankings = result.rankingList;
      this.ignoredColumns = result.ignoredColumns;
      let elem: HTMLElement = document.getElementById('game-name');
      elem.style.fontFamily = this.gameSelected.fontName;
      this.blockUI.stop();
    });
  }

  private getGamesRanks(): void {
    this.http.get<any>('http://localhost:8666/api/rank', {
      params: {
        type: 'GAME'
      }
    }).subscribe((result) => {
      this.rankings = result.rankingList;
      this.ignoredColumns = result.ignoredColumns;
      this.blockUI.stop();
    });
  }

  getGames(): void {
    this.http.get<any[]>('http://localhost:8666/api/games')
      .subscribe((result) => {
        this.games = result;
        this.blockUI.stop();
      });
  }

  private getUsersRanks() {
    this.http.get<any>('http://localhost:8666/api/rank', {
      params: {
        type: 'USER'
      }
    }).subscribe((result) => {
      this.rankings = result.rankingList;
      this.ignoredColumns = result.ignoredColumns;
      this.blockUI.stop();
    });
  }
}
