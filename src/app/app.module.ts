import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatInputModule} from '@angular/material/input';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {KeysPipe} from './utils/keysPipe';
import {MatTableModule} from '@angular/material/table';
import { DynamicTableComponent } from './dynamic-table/dynamic-table.component';
import {MatSortModule} from '@angular/material/sort';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from '@angular/material/tabs';
import { RegisterComponent } from './register/register.component';
import {BlockUIModule} from 'ng-block-ui';
import { GameDialogComponent } from './dialogs/game-dialog/game-dialog.component';
import { UserDialogComponent } from './dialogs/user-dialog/user-dialog.component';
import { MatchDialogComponent } from './dialogs/match-dialog/match-dialog.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {SafeHtmlPipe} from './utils/safeHtmlPipe';

@NgModule({
  declarations: [
    AppComponent,
    AppComponent,
    HomeComponent,
    KeysPipe,
    SafeHtmlPipe,
    DynamicTableComponent,
    RegisterComponent,
    GameDialogComponent,
    UserDialogComponent,
    MatchDialogComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    BrowserAnimationsModule,
    AppRoutingModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatSelectModule,
    MatMenuModule,
    MatDividerModule,
    BrowserModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatExpansionModule,
    HttpClientModule,
    MatSortModule,
    MatIconModule,
    BlockUIModule.forRoot(),
    FormsModule,
    MatPaginatorModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
