import { Component, OnInit } from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {Ranking} from '../models/ranking';
import {FormControl, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {MatDialog} from '@angular/material/dialog';
import {GameDialogComponent} from '../dialogs/game-dialog/game-dialog.component';
import {UserDialogComponent} from '../dialogs/user-dialog/user-dialog.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  @BlockUI() blockUI: NgBlockUI;

  private currentTab: string;
  data: any[];
  games: any[] = [];
  gameSelectControl = new FormControl('', Validators.required);
  gameSelected: any = undefined;
  ignoredColumns = [];
  date: any = new Date();

  constructor(private http: HttpClient, public dialog: MatDialog) {
    this.blockUI.start();
  }

  ngOnInit(): void {
    this.currentTab = 'GAME';
    this.getGames();
  }

  openDialog(event: any): void {
    if (this.currentTab === 'GAME') {
      const dialogRef = this.dialog.open(GameDialogComponent, {
        width: '100%',
        data: event.data
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.changeTable(this.currentTab);
      });
    } else if (this.currentTab === 'GAME_MATCH') {
      console.log("aaaaaaaaaaa");
      } else if (this.currentTab === 'USERS') {
      const dialogRef = this.dialog.open(UserDialogComponent, {
        data: event.data
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.changeTable(this.currentTab);
      });
    }
  }

  changeTable(tab): void {
    this.blockUI.start();
    this.gameSelected = undefined;
    this.currentTab = tab;
    if (tab === 'GAME') {
      this.getGames();
    } else if (tab === 'GAME_MATCH') {
      this.data = [];
    } else if (tab === 'USERS') {
      this.getUsers();
    }
    this.blockUI.stop();
  }

  selectGame(game): void {
    this.blockUI.start();
    this.http.get<any[]>('http://localhost:8666/api/games/matches/' + game.id).subscribe((result) => {
      this.data = result;
      this.blockUI.stop();
    });
  }

  private getUsers(): void {
    this.http.get<any[]>(environment.apiUrl + '/user').subscribe((result) => {
      this.data = [];
      this.data = result;
      this.blockUI.stop();
    });
  }

  getGames(): void {
    this.data = [];
    this.http.get<any[]>('http://localhost:8666/api/games')
      .subscribe((result) => {
        this.data = result;
        this.games = result;
        this.blockUI.stop();
      });
  }

  testLog(event): void {
    console.log(event);
    console.log(event.tab.textLabel);
    console.log(event.index);
  }
}
