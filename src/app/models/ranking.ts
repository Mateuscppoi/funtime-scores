import {RankingType} from './rankingType';

export interface Ranking {
  id: number;
  position: number;
  name: string;
  gameId: number;
  userId: number;
  rankingType: RankingType;
  playersCount: number;
  matchesPlayed: number;
  winsCount: number;
  totalScore: number;

}
