import {Component, HostBinding, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {OverlayContainer} from '@angular/cdk/overlay';
import {MatDialog} from '@angular/material/dialog';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import { name } from '../../package.json';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'funtime-scores';
  active: boolean;
  appName = name;

  @HostBinding('class') className = '';

  toggleControl = new FormControl(false);

  constructor(private dialog: MatDialog,
              private overlay: OverlayContainer,
              private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer) {
    this.active = true;
    this.matIconRegistry.addSvgIcon('game-console', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/game_console.svg'));
    this.matIconRegistry.addSvgIcon('mana-potion', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/mana_potion.svg'));
    this.matIconRegistry.addSvgIcon('death', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/death.svg'));
    this.matIconRegistry.addSvgIcon('handgun', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/handgun.svg'));
    this.matIconRegistry.addSvgIcon('health-potion', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/health_potion.svg'));
    this.matIconRegistry.addSvgIcon('ladder', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ladder.svg'));
    this.matIconRegistry.addSvgIcon('life', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/life.svg'));
    this.matIconRegistry.addSvgIcon('nintendo-switch', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/nintendo_switch.svg'));
    this.matIconRegistry.addSvgIcon('playstation', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/playstation.svg'));
    this.matIconRegistry.addSvgIcon('ps4-controller', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ps4_controller.svg'));
    this.matIconRegistry.addSvgIcon('retro-console', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/retro_console.svg'));
    this.matIconRegistry.addSvgIcon('shield', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/shield.svg'));
    this.matIconRegistry.addSvgIcon('sports', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/sports.svg'));
    this.matIconRegistry.addSvgIcon('steering-wheel', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/steering_wheel.svg'));
    this.matIconRegistry.addSvgIcon('sword', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/sword.svg'));
    this.matIconRegistry.addSvgIcon('xbox-controller', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/xbox_controller.svg'));
    this.matIconRegistry.addSvgIcon('score', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/score.svg'));
    this.matIconRegistry.addSvgIcon('score-controller', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/score_controller.svg'));
    this.matIconRegistry.addSvgIcon('ranking', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/ranking.svg'));
    this.matIconRegistry.addSvgIcon('scoreboard', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/scoreboard.svg'));
    this.matIconRegistry.addSvgIcon('edit', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/edit.svg'));
    this.matIconRegistry.addSvgIcon('warning', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/warning.svg'));
    this.matIconRegistry.addSvgIcon('user', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/user.svg'));
  }

  ngOnInit(): void {
    const darkModeOn = localStorage.getItem('darkTheme');
    if (darkModeOn === 'true') {
      this.className = 'darkMode';
      this.toggleControl.setValue(true);
      this.overlay.getContainerElement().classList.add('darkMode');
    }
    this.toggleControl.valueChanges.subscribe((darkMode) => {
      const darkClassName = 'darkMode';
      this.className = darkMode ? darkClassName : '';
      if (darkMode) {
        this.overlay.getContainerElement().classList.add(darkClassName);
        localStorage.setItem('darkTheme', 'true');
      } else {
        this.overlay.getContainerElement().classList.remove(darkClassName);
        localStorage.setItem('darkTheme', 'false');
      }
    });
  }
}
