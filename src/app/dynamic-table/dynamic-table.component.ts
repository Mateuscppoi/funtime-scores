import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.css']
})
export class DynamicTableComponent implements OnChanges {

  @Input()
  data: any[];

  @Input()
  hasCallback = false;

  @Output()
  callback: EventEmitter<any> = new EventEmitter();

  dataSource: MatTableDataSource<any>;

  @Input()
  ignoredColumns: string[] = [];

  displayedColumns: string[] = [];

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild('dynamicTable') dynamicTableEle: ElementRef;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.data !== undefined) {
      this.displayedColumns = [];

      const keys = this.getTableKeys(this.data[0]);

      this.ignoredColumns.forEach(ignoredColumn => {
        this.data.forEach(object => {
          delete object[ignoredColumn];
        });
      });

      keys.forEach(key => {
        this.displayedColumns.push(key);
      });
      if (this.hasCallback) {
        this.displayedColumns.push('edit');
      }
      this.dataSource = new MatTableDataSource(this.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getTableKeys(value): any {
    const keys = [];
    for (const key in value) {
      const valor = value[key];
      if (valor !== undefined && valor !== null && !this.ignoredColumns.includes(key)) {
        keys.push(key);
      }
    }
    return keys;
  }

  isBoolean(element: any): boolean {
    if (element === undefined) {
      return false;
    }
    return typeof element === 'boolean';
  }

  booleanIcon(element: boolean): string {
    return element ? 'done' : 'clear';
  }

  booleanIconColor(element: boolean): string {
    return element ? 'green' : 'red';
  }

  // printField(element: any, column: any): string {
  //   if (column === 'icon') {
  //     return `<mat-icon  style="width: 32px; height: 32px" svgIcon="${element}"></mat-icon>`;
  //   }
  //
  //   if (this.isBoolean(element)) {
  //     const icon = this.booleanIcon(element);
  //     const iconColor = this.booleanIconColor(element);
  //     return `<mat-icon [ngStyle]="{'color': ${iconColor}">${icon}</mat-icon>`;
  //   }
  //
  //   if (column === 'edit') {
  //     return `<button mat-button (click)="callback.emit({data: element})"><mat-icon>edit</mat-icon></button>`;
  //   }
  //   return `<span style="align-items: center">${element}</span>`;
  // }

  printField(element: any, column: any): boolean {
    if (column === 'icon') {
      return false;
    }

    if (this.isBoolean(element)) {
      return false;
    }

    if (column === 'edit') {
      return false;
    }

    if (this.isDate(element, column)) {
      return false
    }

    return true;
  }


  isDate(elementElement: any, column: string): boolean {
    if (column === 'date') {
      return  true;
    }
    return false;
  }
}
